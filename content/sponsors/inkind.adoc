+++
title = "In-Kind Sponsors"
date = "2021-04-19"
categories = [ "Sponsors" ]
weight = 20
summary = "Sponsors who donate services to help KiCad"
[menu.main]
    parent = "Sponsors"
    name   = "In-Kind Sponsors"
+++

KiCad relies on the support of our community to build and expand the KiCad EDA platform.  We would like to thank the following Corporate Sponsors who have generously donated their services this year.

We encourage corporate users of KiCad to support the community by link:{{% ref path="become-a-sponsor.adoc" %}}[joining our Corporate Sponsorship Program].

{{< sponsors_list title="" filter="inkind">}}

